# GitLab Infrastructure templates

This project is used as a default `Issue` and `MR` template source for the
[`gitlab-com/gl-infra`](https://gitlab.com/gitlab-com/gl-infra) group.

## Template scope

The templates here should be **team agnostic** and useful as a whole to the
[infrastructure
team](https://about.gitlab.com/handbook/engineering/infrastructure/).

Team specific templates can be added in sub-groups and/or projects as needed,
examples:

- [gitlab-com/gl-infra/delivery](https://gitlab.com/gitlab-com/gl-infra/delivery)
- [gitlab-com/gl-infra/gitlab-dedicated/team](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team)
- [gitlab-com/gl-infra/reliability](https://gitlab.com/gitlab-com/gl-infra/reliability)
- [gitlab-com/gl-infra/scalability](https://gitlab.com/gitlab-com/gl-infra/scalability)
